<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session`.
 */
class m181222_005015_create_session_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE session (
            id CHAR(40) NOT NULL PRIMARY KEY,
            expire INTEGER,
            data BLOB
)
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('session');
    }
}
