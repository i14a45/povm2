<?php

use yii\db\Migration;

/**
 * Handles the creation of table `town_photo`.
 */
class m181228_095921_create_town_photo_table extends Migration
{
    public $table = 'town_photo';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(60)->notNull(),
            'town_id'   => $this->integer(),
            'main'      => $this->tinyInteger()->defaultValue(0),
        ]);

        $this->createIndex('town', $this->table, 'town_id');
        $this->createIndex('main', $this->table, 'main');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
