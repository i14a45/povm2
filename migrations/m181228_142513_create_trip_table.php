<?php

use yii\db\Migration;

/**
 * Handles the creation of table `trip`.
 */
class m181228_142513_create_trip_table extends Migration
{
    public $table = '{{%trip}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'            => $this->primaryKey(),
            'type'          => $this->tinyInteger()->notNull()->comment('1-ищу водителя, 2-ищу пассажира'),
            'user_id'       => $this->integer()->notNull(),
            'trip_time'     => $this->dateTime()->notNull(),
            'town_from'     => $this->string(),
            'town_to'       => $this->string(),
            'from_id'       => $this->integer()->notNull(),
            'to_id'         => $this->integer()->notNull(),
            'from_place'    => $this->string()->null(),
            'to_place'      => $this->string()->null(),
            'driver_id'     => $this->integer()->null(),
            'passengers'    => $this->tinyInteger()->notNull()->defaultValue(1),
            'description'   => $this->text(),
            'price'         => $this->integer()->notNull()->defaultValue(0),
            'periodic'      => $this->tinyInteger()->notNull()->defaultValue(0),
            'car_id'        => $this->integer()->null(),
            'views'         => $this->integer()->notNull()->defaultValue(0),
            'status'        => $this->smallInteger()->notNull()->defaultValue(0)->comment('0-неактивная, 10-активная, 20-законченная, -10-удалена'),
            'created_at'    => $this->dateTime()->notNull()->defaultValue(new \yii\db\Expression('current_timestamp()')),
        ]);

        $this->createIndex('user', $this->table, 'user_id');
        $this->createIndex('driver', $this->table, 'driver_id');
        $this->createIndex('from', $this->table, 'from_id');
        $this->createIndex('to', $this->table, 'to_id');
        $this->createIndex('status', $this->table, 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
