<?php

use yii\db\Migration;

/**
 * Class m190210_113002_car_mark_model_tables
 */
class m190210_113002_car_mark_model_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'car_mark_models.sql');
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car_mark}}');
        $this->dropTable('{{%car_model}}');
    }
}
