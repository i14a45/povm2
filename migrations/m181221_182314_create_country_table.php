<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m181221_182314_create_country_table extends Migration
{
    public $table = '{{%geo_country}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()-> notNull(),
            'alias' => $this->string()->notNull(),
        ]);

        $this->createIndex('country_alias', $this->table, 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
