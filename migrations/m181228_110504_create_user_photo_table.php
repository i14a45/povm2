<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_photo`.
 */
class m181228_110504_create_user_photo_table extends Migration
{
    public $table = '{{%user_photo}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(60)->notNull(),
            'user_id'   => $this->integer(),
            'main'      => $this->tinyInteger()->defaultValue(0),
        ]);

        $this->createIndex('user', $this->table, 'user_id');
        $this->createIndex('main', $this->table, 'main');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
