$(function() {
    $("select.select2").select2();

    $("select.select-town").select2({
        minimumInputLength: 3,
        ajax: {
            cache: false,
            allowClear: true,
            dataType: 'json',
            delay: 250,
            type: "POST",
            timeout: 6000,
            data: function(params) {
                return {
                    query: params.term,
                    _csrf: yii.getCsrfToken()
                };
            },
        }
    });


    var $carMark = $("#car-mark");
    var $carModel = $("#car-model");

    $carMark.change(function() {
        var id = $(this).val();
        $carModel.empty();
        $carModel.append('<option value="">- Выбрать -</option>');
        $.post("/car/models/", {id: id}, function(data){
            if (data && data.length > 0) {
                for (var i in data) {
                    $carModel.append(new Option(data[i].name, data[i].id, false, false));
                }
                $carModel.trigger('change');
            }
        }, "json");
    });

    var $step2FormwayBack = $("#step2form-wayback");
    $("#step2form-wayback").change(function() {
        if ($("#step2form-wayback").is(":checked")) {
            $(".date-back").show();
        } else {
            $(".date-back").hide();
        }
    });
    $("#step2form-wayback").change();

});