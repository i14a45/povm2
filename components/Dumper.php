<?php

namespace app\components;


use yii\helpers\VarDumper;

/**
 * Class Dumper
 * @package app\components
 */
class Dumper
{
    /**
     * @param mixed $var
     * @param bool $exit
     */
    public static function dump($var, $exit = true)
    {
        VarDumper::dump($var, 10, true);
        if ($exit) {
            exit;
        }
    }
}