<?php

namespace app\components\services;

use app\models\Trip;
use app\models\User;
use Yii;

/**
 * Class UserService
 * @package app\components\services
 */
class UserService
{
    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User|null
     * @throws \yii\base\Exception
     */
    public static function createUser($name, $email, $password)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password_hash = Yii::$app->security->generatePasswordHash($password);
        $user->confirm_code = Yii::$app->security->generateRandomString(64);
        $user->status = User::STATUS_INACTIVE;

        return $user->save() ? $user : null;
    }

    /**
     * @param User $user
     * @return bool
     */
    public static function confirm(User $user)
    {
        $user->status = User::STATUS_ACTIVE;
        $user->confirm_code = null;
        if ($user->save()) {
            Trip::updateAll(['status' => Trip::STATUS_ACTIVE], ['user_id' => $user->id]);
            return true;
        }

        return false;
    }
}