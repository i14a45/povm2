<?php


namespace app\components\services;


use app\models\Car;

/**
 * Class CarService
 * @package app\components\services
 */
class CarService
{
    /**
     * @param int $carMarkId
     * @param int $carModelId
     * @param int $year
     * @return Car|null
     */
    public static function create($carMarkId, $carModelId, $year)
    {
        $car = new Car();
        $car->mark_id = $carMarkId;
        $car->model_id = $carModelId;
        $car->year = $year;

        return $car->save() ? $car : null;
    }
}