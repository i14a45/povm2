<?php


namespace app\components\services;


use app\models\GeoTown;
use app\models\Trip;

/**
 * Class TripService
 * @package app\components\services
 */
class TripService
{
    /**
     * @param int $userId
     * @param int $type
     * @param int $fromId
     * @param int $toId
     * @param string $date
     * @param int $carId
     * @return Trip|null
     */
    public static function create($userId, $type, $fromId, $toId, $date, $carId)
    {
        $from = GeoTown::findOne($fromId);
        $to = GeoTown::findOne($toId);
        if ($from === null || $to == null) {
            return null;
        }

        $trip = new Trip();
        $trip->type = $type;
        $trip->trip_time = $date;
        $trip->town_from = $from->name;
        $trip->from_id = $from->id;
        $trip->town_to = $to->name;
        $trip->to_id = $to->id;
        $trip->user_id = $userId;
        $trip->car_id = $carId;
        $trip->status = Trip::STATUS_INACTIVE;

        return $trip->save() ? $trip : null;
    }

    /**
     * @param Trip $trip
     */
    public static function incrementViews(Trip $trip)
    {
        $trip->updateCounters(['views' => 1]);
    }
}