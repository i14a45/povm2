<?php

namespace app\controllers;

use app\components\services\UserService;
use app\models\ContactForm;
use app\models\GeoTown;
use app\models\LoginForm;
use app\models\RegistrationForm;
use app\models\RestorePasswordForm;
use app\models\SearchForm;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\captcha\CaptchaAction;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\Response;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                    'town' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
            'captcha' => [
                'class' => CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchForm = new SearchForm();

        return $this->render('index', [
            'searchForm' => $searchForm,
        ]);
    }

    /**
     * @return array
     */
    public function actionTown()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return GeoTown::findByTerm(Yii::$app->request->post('query'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionRestorePassword()
    {
        $model = new RestorePasswordForm();

        if ($model->load(Yii::$app->request->post()) && $model->resetPassword() && $model->sendLetter()) {
            return $this->render('restore-letter-sent');
        }

        return $this->render('restore-password', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionRegister()
    {
        $model = new RegistrationForm();

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            $user = $model->getUser();
            Yii::$app->session->setFlash('success', 'На адрес ' . $user->email . ' отправлено письмо с инструкцией для подтверждения регистрации');
            return $this->redirect(['/site/index']);
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $email
     * @param string $code
     * @return Response
     */
    public function actionConfirm($email, $code)
    {
        $user = User::findOne([
            'email' => $email,
            'confirm_code' => $code,
            'status' => User::STATUS_INACTIVE
        ]);

        if ($user === null) {
            Yii::$app->session->setFlash('error', 'Пользователь с такими данными не найден, либо не требует активации');
            return $this->redirect(['index']);
        }

        if (UserService::confirm($user)) {
            Yii::$app->session->setFlash('success', 'Ваш аккаунт активирован');
            Yii::$app->user->login($user);
            return $this->redirect(['/profile/view']);
        }

        Yii::$app->session->setFlash('error', 'Возникла непредвиденная ошибка. Обратитесь в службу поддержки');
        return $this->redirect(['index']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionCalculator()
    {
        return $this->render('calculator');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionFeedback()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('feedback', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionSafety()
    {
        return $this->render('safety');
    }

    /**
     * @return string
     */
    public function actionRules()
    {
        return $this->render('rules');
    }

    /**
     * @return string
     */
    public function actionPartners()
    {
        return $this->render('partners');
    }
}
