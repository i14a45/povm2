<?php

namespace app\controllers;


use app\components\Dumper;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ProfileController
 * @package app\controllers
 */
class ProfileController extends Controller
{
    /**
     * @param int|null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id = null)
    {
        if (!$id && Yii::$app->user->isGuest) {
            throw new NotFoundHttpException();
        }

        if ($id === null) {
            $id = (int) Yii::$app->user->id;
        }

        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionBlock()
    {

    }

    public function actionUpdate()
    {

    }

    /**
     * @param $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = User::find()->where(['id' => $id, 'status' => User::STATUS_ACTIVE])->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}