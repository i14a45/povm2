<?php

namespace app\controllers;


use app\components\services\TripService;
use app\models\Step2Form;
use app\models\Step3Form;
use Yii;
use app\components\Dumper;
use app\models\GeoCountry;
use app\models\GeoRegion;
use app\models\GeoTown;
use app\models\Step1Form;
use app\models\Trip;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class TripsController
 * @package app\controllers
 */
class TripsController extends Controller
{
    public function actionSearch()
    {

    }

    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionCreate()
    {
        $step = Yii::$app->session->get('step', 1);

        // сверяем шаг в сессии и в ссылке
        $get = Yii::$app->request->get();
        for ($i = 1; $i <= 3; $i++) {
            if ($step == $i && !array_key_exists("step$i/", $get)) {
                // если не совпадает, редиректим на нужный
                return $this->redirect(["/trips/create/?step$step/"]);
            }
        }

        switch ($step) {
            case 2:
                $model = new Step2Form();
                break;
            case 3:
                $model = new Step3Form();
                break;
            default:
                $model = new Step1Form();
        }

        if (Yii::$app->request->isGet && $step !== 1) {
            $model->attributes = Yii::$app->session->get('stepForm');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($step === 3 && $model->process()) {
                $user = $model->getUser();
                Yii::$app->session->remove('step');
                Yii::$app->session->remove('stepForm');
                if ($user) {
                    Yii::$app->session->setFlash('success', 'На адрес ' . $user->email . ' отправлено письмо с инструкцией для подтверждения поездки');
                } else {
                    Yii::$app->session->setFlash('error', $model->getError());
                }
                return $this->redirect('/');
            }
            $step++;
            // пишем шаг в сессию
            Yii::$app->session->set('step', $step);
            // пишем в сессию данные модели
            Yii::$app->session->set('stepForm', $model->attributes);
            return $this->redirect(["/trips/create/?step$step/"]);
        }

        return $this->render('step' . $step, [
            'model' => $model,
        ]);
    }

    /**
     * @param $country
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCountry($country)
    {
        $countryModel = $this->getCountryByAlias($country);

        return $this->render('country', [
            'country' => $countryModel,
            'region'  => null,
            'town'    => null,
        ]);
    }

    /**
     * @param $country
     * @param $region
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRegion($country, $region)
    {
        $countryModel = $this->getCountryByAlias($country);
        $regionModel = $this->getRegionByAlias($region);

        return $this->render('region', [
            'country' => $countryModel,
            'region'  => $regionModel,
            'town'    => null,
        ]);
    }

    /**
     * @param $country
     * @param $region
     * @param $town
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTown($country, $region, $town)
    {
        $countryModel = $this->getCountryByAlias($country);
        $regionModel = $this->getRegionByAlias($region);
        $townModel = $this->getTownByAlias($town);

        return $this->render('town', [
            'country' => $countryModel,
            'region'  => $regionModel,
            'town'    => $townModel,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = Trip::findById($id);

        $this->checkNull($model);

        TripService::incrementViews($model);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $alias
     * @return GeoCountry
     * @throws NotFoundHttpException
     */
    protected function getCountryByAlias($alias)
    {
        $model = GeoCountry::getByAlias($alias);

        $this->checkNull($model);

        return $model;
    }

    /**
     * @param $alias
     * @return GeoRegion
     * @throws NotFoundHttpException
     */
    protected function getRegionByAlias($alias)
    {
        $model = GeoRegion::getByAlias($alias);

        $this->checkNull($model);

        return $model;
    }

    /**
     * @param $alias
     * @return GeoTown
     * @throws NotFoundHttpException
     */
    protected function getTownByAlias($alias)
    {
        $model = GeoTown::getByAlias($alias);

        $this->checkNull($model);

        return $model;
    }

    /**
     * @param ActiveRecord $model
     * @throws NotFoundHttpException
     */
    protected function checkNull($model)
    {
        if ($model === null) {
            throw new NotFoundHttpException();
        }
    }

}