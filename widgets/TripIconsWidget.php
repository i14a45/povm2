<?php

namespace app\widgets;


use app\models\Trip;
use yii\base\InvalidArgumentException;
use yii\base\Widget;

/**
 * Class TripIconsWidget
 * @package app\widgets
 */
class TripIconsWidget extends Widget
{
    /** @var Trip */
    public $model;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->model instanceof Trip) {
            parent::init();
        } else {
            throw new InvalidArgumentException('User can\'t be empty');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('trip-icons', [
            'model' => $this->model,
        ]);
    }
}