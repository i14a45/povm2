<?php

namespace app\widgets;


use app\models\Car;
use yii\base\InvalidArgumentException;
use yii\base\Widget;

/**
 * Class CarGalleryWidget
 * @package app\widgets
 */
class CarGalleryWidget extends Widget
{
    /**
     * @var Car
     */
    public $car;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->car instanceof Car) {
            parent::init();
        } else {
            throw new InvalidArgumentException('User can\'t be empty');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('car-gallery', [
            'car' => $car,
        ]);
    }
}