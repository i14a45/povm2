<?php

use app\components\StringHelper;
use yii\helpers\Html;
use app\models\Trip;

/* @var $this yii\web\View */
/* @var $model \app\models\Trip */

if ($model->type === Trip::TYPE_FIND_DRIVER) {
    $title = 'Пассажиры ищут водителя';
    $icon = '<i class="fas fa-walking"></i>';
} elseif ($model->type === Trip::TYPE_FIND_PASSENGERS) {
    $title = 'Водитель ищет пассажиров';
    $icon = '<i class="fas fa-car"></i>';
}

?>

<div class="trip-history-item">
    <div class="trip-icon text-center">
        <?= $icon; ?>
    </div>
    <div class="trip-details">
        <p class="trip-date">
            <?= StringHelper::mb_ucfirst(Yii::$app->formatter->asDate($model->trip_time, 'php:D d M Y')); ?>
        </p>
        <p class="trip-history-link">
            <?= Html::a(Html::encode($model->town_from) . ' &rarr; ' .  Html::encode($model->town_to), ['/trips/view', 'id' => $model->id]); ?>
        </p>
        <p>
            <?= $title; ?>
        </p>
    </div>
    <div class="clr"></div>
</div>
