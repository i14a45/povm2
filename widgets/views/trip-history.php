<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

?>

<h2 style="margin-left:12px;">История поездок</h2>

<div class="trip-history">
    <?php Pjax::begin([
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_trip_history',
        'summary' => false,
        'pager' => [
            'maxButtonCount' => 5,
            'options' => [
                'class' => 'pagination pagination-sm',
                'style' => 'float:right',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>