<?php

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $dataProvider */
/* @var string $title */

?>

<div class="trips" style="margin-top:40px;">
    <?php if ($title) { ?>
        <h2><?= $title; ?></h2>
    <?php } ?>
    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_trip',
        'summary' => false,
    ]); ?>
</div>
