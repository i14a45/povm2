<?php

use app\components\StringHelper;
use yii\helpers\Html;
use app\models\Trip;
use yii\i18n\MessageFormatter;

/* @var \yii\web\View $this */
/* @var \app\models\Trip $model */

$lookingFor = '';

if ($model->type === Trip::TYPE_FIND_DRIVER) {
    $title = 'Пассажиры ищут водителя';
    $lookingFor = 'Ищу водителя';
} elseif ($model->type === Trip::TYPE_FIND_PASSENGERS) {
    $title = 'Водитель ищет пассажиров';
    $lookingFor = 'Ищу пассажира';
}
?>

<div class="trips-list-item well">
    <div class="row">
        <div class="col-md-2">
            <div class="text-center">
                <?= Html::a(
                    Html::img('/img/thumbs/user.jpg', [
                        'class' => 'user-avatar',
                        'alt' => Html::encode($model->user ? $model->user->name : 'unknown'),
                    ]), ['/user/view', 'id' => $model->user->id]
                ); ?>
                <div class="looking-for">
                    <?= $lookingFor; ?>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="trip-header">
                <div class="trip-date pull-left">
                    <?= StringHelper::mb_ucfirst(Yii::$app->formatter->asDate($model->trip_time, 'php:D d M Y')); ?>
                </div>
                <div class="trip-title pull-right">
                    <?= Html::a(Html::encode($model->town_from) . '&nbsp;&rarr;&nbsp;' .  Html::encode($model->town_to), ['/trips/view', 'id' => $model->id]); ?>
                </div>
                <div class="clr"></div>
            </div>
            <div class="tip-content">
                <?= \app\widgets\TripIconsWidget::widget([
                    'model' => $model,
                ]); ?>
                <div class="panel panel-default trip-description">
                    <div class="panel-body">
                        <?= empty($model->description) ? 'Нет описания' : Html::encode($model->description); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

