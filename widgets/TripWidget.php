<?php

namespace app\widgets;


use app\models\Trip;
use app\models\TripSearch;
use yii\base\Widget;

class TripWidget extends Widget
{
    public $title;
    public $type;
    public $tripTime;
    public $townFrom;
    public $townTo;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $params = [
            'type' => $this->type,
            'tripTime' => $this->tripTime,
            'townFrom' => $this->townFrom,
            'townTo' => $this->townTo,
            'status' => [Trip::STATUS_ACTIVE, Trip::STATUS_FINISHED],
        ];
        $searchModel = new TripSearch();
        $dataProvider = $searchModel->search($params);

        return $this->render('trips', [
            'dataProvider' => $dataProvider,
            'title' => $this->title,
        ]);
    }
}
