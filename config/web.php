<?php

use yii\web\UrlNormalizer;
use yii\debug\Module;
use yii\caching\FileCache;
use app\models\User;
use yii\swiftmailer\Mailer;
use yii\log\FileTarget;
use yii\web\DbSession;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'povm2',
    'language' => 'ru',
    'name' => 'Сервис поиска попутчиков &laquo;Поехали вместе!&raquo;',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@res'   => '@vendor/../res',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '6m6A-GMpybKU8ac862X9dxGf1rlKo3b7',
        ],
        'cache' => [
            'class' => FileCache::class,
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => Mailer::class,
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => YII_DEBUG ? true : false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class'  => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'suffix' => '/',
            'normalizer' => [
                'class' => UrlNormalizer::class,
                'collapseSlashes' => true,
                'normalizeTrailingSlash' => true,
            ],
            'rules' => [
                '/<country:(belarus|russia|ukraine)>'                                   => '/trips/country',
                '/<country:(belarus|russia|ukraine)>/<region:[\w\-]+>'                  => '/trips/region',
                '/<country:(belarus|russia|ukraine)>/<region:[\w\-]+>/<town:[\w\-]+>'   => '/trips/town',
                '/search'                                                               => '/trips/search',
                '/trip/<id:\d+>'                                                        => '/trips/view',
                '/trip'                                                                 => '/trips/index',
                '/logout' => '/site/logout',
                '/<action:(login|feedback|about|safety|rules|partners|town)>'    => '/site/<action>',
                '/car/<id:\d+>'                                                         => '/auto/view',
                '/car'                                                                  => '/auto/index',
                '/car/create'                                                           => '/auto/create',
                '/car/models'                                                           => '/auto/models',
                '/car/<action:(update|delete)>/<id:\d+>'                                => '/auto/<action>',
                '/user'                                                                 => '/profile/view',
                '/user/<id:\d+>'                                                        => '/profile/view',
                '/user/<action:(update|block|change-password)>'                         => '/profile/<action>',


            ],
        ],
        'session' => [
            'class' => DbSession::class,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => Module::class,
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => \yii\gii\Module::class,
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
