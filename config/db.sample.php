<?php

use yii\db\Connection;

if (YII_DEBUG) {
    return [
        'class' => Connection::class,
        'dsn' => 'mysql:host=localhost;dbname=povm2',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',

        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
    ];
} else {
    return [
        'class' => Connection::class,
        'dsn' => 'mysql:host=localhost;dbname=povm2',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',

        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
    ];
}
