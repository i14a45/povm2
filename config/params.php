<?php

return [
    'maxCarAge' => 30, // максимальный возраст в машине (для поля год выпуска)

    'adminEmail' => 'admin@example.com',
    'fromEmail' => 'mail@povm.ru',
];
