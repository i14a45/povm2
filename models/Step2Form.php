<?php


namespace app\models;


use yii\helpers\ArrayHelper;

/**
 * Class Step2Form
 * @package app\models
 */
class Step2Form extends Step1Form
{
    /** @var integer */
    public $fromId;

    /** @var integer */
    public $toId;

    /** @var string */
    public $date;

    /** @var integer */
    public $carMarkId;

    /** @var integer */
    public $carModelId;

    /** @var integer */
    public $carYear;

    /** @var boolean */
    public $wayBack;

    /** @var string */
    public $dateBack;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['fromId', 'toId', 'date'], 'required'],
            [['fromId', 'toId'], 'exist', 'targetClass' => GeoTown::class, 'targetAttribute' => 'id'],
            [['carMarkId', 'carModelId', 'carYear'], 'required', 'when' => function($model) {
                /** @var self $model */
                return $model->type == Trip::TYPE_FIND_PASSENGERS;
            }, 'whenClient' => 'function(attr, val){return $("#step2form-type") == 2}'],
            [['carMarkId'], 'exist', 'targetClass' => CarMark::class, 'targetAttribute' => 'id'],
            [['carModelId'], 'exist', 'targetClass' => CarModel::class, 'targetAttribute' => 'id'],
            [['date'], 'validateDateTime', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['dateBack'], 'validateDateTime', 'skipOnEmpty' => false, 'skipOnError' => false, 'when' => function($model) {
                /** @var self $model */
                return $model->wayBack;
            }],
            [['wayBack'], 'in' ,'range' => [0, 1]],
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateDateTime($attribute, $params)
    {
        if (\DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $this->$attribute)) == false) {
            $this->addError($attribute, 'Неверно заполнены дата/время');
        }

    }

    public function attributeLabels()
    {
        return [
            'fromId' => 'Откуда едем',
            'toId' => 'Куда едем',
            'date' => 'Дата/время выезда',
            'carMarkId' => 'Марка автомобиля',
            'carModelId' => 'Модель автомобиля',
            'carYear' => 'Год выпуска',
            'wayBack' => 'Обратно',
            'dateBack' => 'Дата/время выезда',
        ];
    }
}