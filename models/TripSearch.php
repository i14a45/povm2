<?php

namespace app\models;


use yii\data\ActiveDataProvider;

class TripSearch extends Trip
{
    public $tripTime;
    public $townFrom;
    public $townTo;
    public $tripStatus;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => array_keys(static::getTypeList())],
            [['tripStatus'], 'in', 'range' => array_keys(static::getStatusList())],
            [['tripTime'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder'  => ['trip_time' => SORT_ASC]],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['type' => $this->type]);
        $tripTime = date('Y-m-d H:i:s');
        if ($this->tripTime && $this->tripTime > date('Y-m-d H:i:s')) {
            $tripTime = $this->tripTime;
        }
        $query->andFilterWhere(['>=', 'trip_time', $tripTime]);

        $query->andWhere(['in', 'status', [static::STATUS_ACTIVE, static::STATUS_FINISHED]]);

        return $dataProvider;
    }
}