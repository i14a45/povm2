<?php

namespace app\models;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $password_hash
 * @property string $email
 * @property string $confirm_code
 * @property string $hash
 * @property string $login
 * @property string $description
 * @property int $gender
 * @property string $role
 * @property int $rating
 * @property int $vk_id
 * @property int $fb_id
 * @property string $car
 * @property string $phone
 * @property int $town_id
 * @property int $phone_access
 * @property int $email_access
 * @property int $subscribe
 * @property string $created_at
 * @property int $status
 * @property string $last_login
 *
 * @property string $authKey
 *
 * @property GeoTown $town
 * @property Car[] $cars
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 10;
    public const STATUS_BANNED = 20;
    public const STATUS_DELETED = -10;

    public const ACCESS_NONE = 0;
    public const ACCESS_REGISTERED = 1;
    public const ACCESS_ALL = 2;

    public const SUBSCRIBE_INACTIVE = 0;
    public const SUBSCRIBE_ACTIVE = 1;

    public const GENDER_MALE = 0;
    public const GENDER_FEMALE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'password_hash', 'email'], 'required'],
            [['email'], 'unique'],
            [['description'], 'string'],
            [['gender', 'rating', 'vk_id', 'fb_id', 'town_id', 'status'], 'integer'],
            [['created_at', 'last_login'], 'safe'],
            [['name', 'password_hash', 'email', 'confirm_code', 'hash'], 'string', 'max' => 255],
            [['login', 'role'], 'string', 'max' => 30],
            [['car'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 16],
            [['phone_access', 'email_access'], 'in', 'range' => [static::ACCESS_NONE, static::ACCESS_REGISTERED, static::ACCESS_ALL]],
            [['subscribe'], 'in', 'range' => [static::SUBSCRIBE_INACTIVE, static::SUBSCRIBE_ACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'password_hash' => 'Password Hash',
            'email' => 'Email',
            'confirm_code' => 'Confirm Code',
            'hash' => 'Hash',
            'login' => 'Login',
            'description' => 'Description',
            'gender' => 'Gender',
            'role' => 'Role',
            'rating' => 'Rating',
            'vk_id' => 'Vk ID',
            'fb_id' => 'Fb ID',
            'car' => 'Car',
            'phone' => 'Phone',
            'town_id' => 'Town ID',
            'created_at' => 'Created At',
            'status' => 'Status',
            'last_login' => 'Last Login',
        ];
    }

    /**
     * @param $email
     * @return User|array|\yii\db\ActiveRecord|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => static::STATUS_ACTIVE]);
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {

        // Старый вариант Yii1
        $salt = substr($this->password_hash, 0, 64); //get the salt from the front of the hash
        $validHash = substr($this->password_hash, 64, 64); //the SHA256

        $testHash = hash('sha256', $salt . $password); //hash the password being tested

        //if the hashes are exactly the same, the password is valid
        if ($testHash === $validHash) {
            return true;
        }

        // Новый вариант Yii2
        $result = false;
        try {
            $result = Yii::$app->security->validatePassword($password, $this->password_hash);
        } catch (InvalidArgumentException$e) {
            return false;
        }
        return $result;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTown()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'town_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::class, ['user_id' => 'id'])->where(['status' => Car::STATUS_ACTIVE]);
    }
}
