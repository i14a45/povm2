<?php

namespace app\models;


use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "geo_town".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $country_id
 * @property int $region_id
 * @property string $description
 * @property int $size
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $photo
 * @property string $lat
 * @property string $lng
 * @property int $is_capital
 *
 * @property GeoCountry $country
 * @property GeoRegion $region
 */
class GeoTown extends \yii\db\ActiveRecord implements GeoInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_town';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'country_id', 'region_id'], 'required'],
            [['country_id', 'region_id', 'size', 'is_capital'], 'integer'],
            [['description'], 'string'],
            [['lat', 'lng'], 'number'],
            [['name', 'alias', 'seo_title', 'seo_description', 'seo_keywords', 'photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'description' => 'Description',
            'size' => 'Size',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'photo' => 'Photo',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'is_capital' => 'Is Capital',
        ];
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        return ['/trips/town', 'country' => $this->country->alias, 'region' => $this->region->alias, 'town' => $this->alias];
    }

    /**
     * @param string $alias
     * @return GeoTown|array|\yii\db\ActiveRecord|null
     */
    public static function getByAlias($alias)
    {
        return static::find()->where(['alias' => $alias])->one();
    }

    /**
     * @param string $query
     * @return array
     */
    public static function findByTerm($query)
    {
        $list = static::find()->where(['like', 'name', $query . '%', false])->all();
        $result = [];
        foreach ($list as $item) {
            $result[] = [
                'id' => $item->id,
                'text' => $item->name,
            ];
        }
        return ['results' => $result];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::class, ['id' => 'region_id']);
    }
}
