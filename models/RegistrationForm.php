<?php

namespace app\models;


use app\components\services\UserService;
use Yii;
use yii\base\Model;

/**
 * Class RegistrationForm
 * @package app\models
 */
class RegistrationForm extends Model
{
    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var User */
    private $_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['password'], 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeHints()
    {
        return [
            //'e-mail' => 'E-mail',
            'password' => 'Минимум 6 символов',
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function process()
    {
        // создаем юзера
        $this->_user = UserService::createUser($this->name, $this->email, $this->password);
        if ($this->_user === null) {
            $this->_error = 'Произошла непредвиденная ошибка';
            return false;
        }

        $mailSent = Yii::$app->mailer->compose('register', [
            'email'       => $this->_user->email,
            'confirmCode' => $this->_user->confirm_code,
        ])
            ->setFrom(Yii::$app->params['fromEmail'])
            ->setTo($this->_user->email)
            ->setSubject('Сервис "Поехали вместе!" - Регистрация пользователя')
            ->send();

        if (!$mailSent) {
            $this->_error .= '<br>Произошла ошибка при отправке письма. Обратитесь в службу поддержки';
            return false;
        }
        return true;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }
}