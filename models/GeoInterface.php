<?php

namespace app\models;


use yii\db\ActiveRecord;

interface GeoInterface
{
    /**
     * @param string $alias
     * @return ActiveRecord|null
     */
    public static function getByAlias($alias);

    /**
     * @return array
     */
    public function getUrl();
}