<?php


namespace app\models;


use yii\base\Model;

class Step1Form extends Model
{
    /**
     * @var int
     */
    public $type;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'in', 'range' => [Trip::TYPE_FIND_DRIVER, Trip::TYPE_FIND_PASSENGERS]],
        ];
    }
}