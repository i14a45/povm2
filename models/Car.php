<?php

namespace app\models;

use app\components\StringHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property int $mark_id
 * @property int $model_id
 * @property string $manufacturer
 * @property string $model
 * @property int $year
 * @property int $user_id
 * @property double $pollution
 * @property string $consumption
 * @property string $name
 * @property int $capacity
 * @property string $description
 * @property int $status
 *
 * @property User $user
 * @property CarMark $carMark
 * @property CarModel $carModel
 */
class Car extends ActiveRecord
{
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 10;
    public const STATUS_DELETED = -10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark_id', 'model_id', 'year'], 'required'],
            [['year'], 'integer', 'min' => 1990, 'max' => (int) date('Y')],
            [['capacity'], 'integer', 'min' => 1],
            [['pollution', 'consumption'], 'number'],
            [['description'], 'string'],
            [['manufacturer', 'model'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 255],

            [['pollution', 'consumption'], 'default', 'value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark_id' => 'Марка',
            'model_id' => 'Модель',
            'year' => 'Год',
            'user_id' => 'User ID',
            'pollution' => 'Pollution',
            'consumption' => 'Consumption',
            'name' => 'Name',
            'capacity' => 'Количество мест',
            'description' => 'Описание',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'manufacturer' => 'Например, Mini',
            'model' => 'Например, Cooper JCW',
            'year' => 'Например, 2010',
            'capacity' => 'Например, 3',
            'description' => 'Расскажите немного о своем авто – насколько он хорош?',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // заполняем текст марки и модели
        $this->manufacturer = $this->carMark->name;
        $this->model = $this->carModel->name;

        if ($insert) {
            $this->user_id = Yii::$app->user->id;
            $this->status = static::STATUS_ACTIVE;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getYears()
    {
        $currentYear = (int) date('Y');
        $startYear = $currentYear - Yii::$app->params['maxCarAge'] + 1;

        $range = range($currentYear, $startYear);

        return array_combine($range, $range);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return Html::encode(StringHelper::mb_ucfirst($this->manufacturer)) . ' ' . Html::encode(StringHelper::mb_ucfirst($this->model));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMark()
    {
        return $this->hasOne(CarMark::class, ['id' => 'mark_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::class, ['id' => 'model_id']);
    }
}
