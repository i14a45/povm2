<?php

namespace app\models;

use yii\data\ActiveDataProvider;

/**
 * Class TripHistorySearch
 * @package app\models
 */
class TripHistorySearch extends Trip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder'  => ['trip_time' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere(['or', ['user_id' => $this->user_id], ['driver_id' => $this->user_id]]);

        //$query->andWhere(['in', 'status', [static::STATUS_ACTIVE, static::STATUS_FINISHED]]);

        return $dataProvider;
    }
}