<?php

namespace app\models;


/**
 * This is the model class for table "geo_country".
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 *
 * @property GeoRegion[] $regions
 */
class GeoCountry extends \yii\db\ActiveRecord implements GeoInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'alias' => 'Alias',
        ];
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        return ['/trips/country', 'country' => $this->alias];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(GeoRegion::class, ['country_id' => 'id'])->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @param string $alias
     * @return GeoCountry|array|\yii\db\ActiveRecord|null
     */
    public static function getByAlias($alias)
    {
        return static::find()->where(['alias' => $alias])->one();
    }

    /**
     * @return GeoCountry[]|array|\yii\db\ActiveRecord[]
     */
    public static function getList()
    {
        return static::find()->orderBy(['name' => SORT_ASC])->all();
    }
}
