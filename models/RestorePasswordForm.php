<?php

namespace app\models;


use Yii;
use yii\base\Model;

/**
 * Class RestorePasswordForm
 * @package app\models
 */
class RestorePasswordForm extends Model
{
    /** @var string */
    public $email;

    /** @var User */
    private $_user;

    /** @var string */
    private $_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'validateEmail'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params)
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addError($attribute, 'Пользователь с таким e-mail не найден');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function resetPassword()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $this->_password = Yii::$app->security->generateRandomString(6);
            $passwordHash = Yii::$app->security->generatePasswordHash($this->_password);
            if ($user->updateAttributes(['password_hash' => $passwordHash])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function sendLetter()
    {
        $user = $this->getUser();

        return Yii::$app->mailer->compose('restore-password', ['password' => $this->_password])
            ->setFrom(Yii::$app->params['fromEmail'])
            ->setTo($user->email)
            ->setSubject('Сервис "Поехали вместе!" - Смена пароля пользователя')
            ->send();
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}