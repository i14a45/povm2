<?php


namespace app\models;


use app\components\services\CarService;
use app\components\services\TripService;
use app\components\services\UserService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Step3Form extends Step2Form
{
    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var User */
    private $_user;

    /** @var string */
    private $_error;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'email', 'password'], 'required'],
            [['email'], 'email'],
            [['name'], 'string', 'min' => 4],
            [['password'], 'string', 'min' => 6],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function process()
    {
        // создаем юзера
        $this->_user = UserService::createUser($this->name, $this->email, $this->password);
        if ($this->_user === null) {
            $this->_error = 'Произошла непредвиденная ошибка';
            return false;
        }

        $carId = null;
        if ($this->type === Trip::TYPE_FIND_PASSENGERS) {
            // если надо, создаем машину
            $car = CarService::create($this->carMarkId, $this->carModelId, $this->carYear);
            $carId = $this->_car ? $this->_car->id : null;
            if ($car === null) {
                $this->error .= 'Непредвиденная ошибка при сохранении автомобиля';
            }
        }

        // создаем поездку(и)
        $date = \DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $this->date))->format('Y-m-d H:i:s');
        $trip = TripService::create($this->_user->id, $this->type, $this->fromId, $this->toId, $date, $carId);
        if ($trip === null) {
            $this->_error .= '<br>Непредвиденная ошибка при сохранении поездки';
        }
        if ($trip && $this->wayBack) {
            $dateBack = \DateTime::createFromFormat('d.m.Y H:i', str_replace('+', ' ', $this->dateBack))->format('Y-m-d H:i:s');
            $tripBack = TripService::create($this->_user->id, $this->type, $this->toId, $this->fromId, $dateBack, $carId);
            if ($tripBack === null) {
                $this->_error .= '<br>Непредвиденная ошибка при сохранении поездки';
            }
        }

        $mailSent = Yii::$app->mailer->compose('register', [
            'email'       => $this->_user->email,
            'confirmCode' => $this->_user->confirm_code,
        ])
            ->setFrom(Yii::$app->params['fromEmail'])
            ->setTo($this->_user->email)
            ->setSubject('Сервис "Поехали вместе!" - Регистрация пользователя')
            ->send();

        if (!$mailSent) {
            $this->_error .= '<br>Произошла ошибка при отправке письма. Обратитесь в службу поддержки';
            return false;
        }
        return true;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }
}