<?php

namespace app\models;


/**
 * This is the model class for table "trip".
 *
 * @property int $id
 * @property int $type 1-ищу водителя, 2-ищу пассажира
 * @property int $user_id
 * @property string $trip_time
 * @property string $town_from
 * @property string $town_to
 * @property int $from_id
 * @property int $to_id
 * @property string $from_place
 * @property string $to_place
 * @property int $driver_id
 * @property int $passengers
 * @property string $description
 * @property int $price
 * @property int $periodic
 * @property int $car_id
 * @property int $views
 * @property int $status 0-неактивная, 10-активная, 20-законченная, -10-удалена
 * @property string $created_at
 *
 * @property GeoTown $from
 * @property GeoTown $to
 * @property Car $car
 * @property User $user
 */
class Trip extends \yii\db\ActiveRecord
{
    public const TYPE_FIND_DRIVER = 1;
    public const TYPE_FIND_PASSENGERS = 2;

    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 10;
    public const STATUS_FINISHED = 20;
    public const STATUC_DELETED = -10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'user_id', 'trip_time', 'from_id', 'to_id'], 'required'],
            [['type'], 'in', 'range' => array_keys(static::getTypeList())],
            [['user_id', 'from_id', 'to_id', 'driver_id', 'price', 'car_id'], 'integer'],
            [['passengers'], 'integer', 'min' => 1],
            [['trip_time'], 'safe'],
            [['description'], 'string'],
            [['town_from', 'town_to', 'from_place', 'to_place'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'user_id' => 'User ID',
            'trip_time' => 'Trip Time',
            'town_from' => 'Town From',
            'town_to' => 'Town To',
            'from_id' => 'From ID',
            'to_id' => 'To ID',
            'from_place' => 'From Place',
            'to_place' => 'To Place',
            'driver_id' => 'Driver ID',
            'passengers' => 'Passengers',
            'description' => 'Description',
            'price' => 'Price',
            'periodic' => 'Periodic',
            'car_id' => 'Car ID',
            'views' => 'Views',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array
     */
    public static function getTypeList()
    {
        return [
            static::TYPE_FIND_DRIVER => 'Ищу водителя',
            static::TYPE_FIND_PASSENGERS => 'Ищу пассажиров',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            static::STATUS_INACTIVE => 'Неактивная',
            static::STATUS_ACTIVE => 'Активная',
            static::STATUS_FINISHED => 'Завершенная',
            static::STATUC_DELETED => 'Удаленная',
        ];
    }

    /**
     * @param $id
     * @return Trip|null
     */
    public static function findById($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->andWhere(['in', 'status', [static::STATUS_ACTIVE, static::STATUS_FINISHED]])
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(GeoTown::class, ['id' => 'to_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
