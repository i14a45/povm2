<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $email string */
/* @var $confirmCode string */

$confirmUrl = Url::to(['/site/confirm', 'email' => $email, 'code' => $confirmCode], true);
$confirmLink = Html::a($confirmUrl, $confirmUrl);

?>

<h1>Регистрация на сайте «Поехали вместе!»</h1>
<p>Здравствуйте!<br />
    Вы зарегистрировались на сайте поиска попутчиков <?= Html::a('Поехали вместе!', '/') ?></p>
<p>Для того, чтобы начать пользоваться всеми возможностями сайта, необходимо подтвердить свой email. Для этого перейдите по ссылке:</p>
<p><strong><?= $confirmLink ?></strong></p>
