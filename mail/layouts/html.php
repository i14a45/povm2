<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <style>
        body {
            background: #eee;
            font-family:Helvetica, Arial;
            padding:0;
            margin:0;
        }

        #main-wrapper {
            background: #fff;
            width:90%;
            margin:0 auto;
            height:100%;
            border-right: #ddd 1px solid;
            border-left: #ddd 1px solid;
            color:#444;
            padding:0 10px;
        }

        #header {
            margin-bottom:30px;
        }

        h1,h2,h3,h4,h5,h6 {
            font-weight:normal;
        }
    </style>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div id="main-wrapper">
        <table width="100%" border="0" cellpadding="5" cellspacing="5">
            <tr>
                <td width="20%"><a href='http://www.povm.ru/?utm_medium=email&utm_source=message_header'><img src="http://povm.ru/img/logo_bar.png" alt="&#1055;&#1086;&#1077;&#1093;&#1072;&#1083;&#1080; &#1042;&#1084;&#1077;&#1089;&#1090;&#1077; - &#1087;&#1086;&#1080;&#1089;&#1082; &#1087;&#1086;&#1087;&#1091;&#1090;&#1095;&#1080;&#1082;&#1086;&#1074; &#1074; &#1089;&#1086;&#1074;&#1084;&#1077;&#1089;&#1090;&#1085;&#1099;&#1077; &#1087;&#1086;&#1077;&#1079;&#1076;&#1082;&#1080;" width="209" height="60" align="middle" /></a></td>
                <td width="33%"><h3>&#1055;&#1086;&#1080;&#1089;&#1082; &#1087;&#1086;&#1087;&#1091;&#1090;&#1095;&#1080;&#1082;&#1086;&#1074; &#1074; &#1089;&#1086;&#1074;&#1084;&#1077;&#1089;&#1090;&#1085;&#1099;&#1077; &#1087;&#1086;&#1077;&#1079;&#1076;&#1082;&#1080;!</h3></td>
                <td width="33%"></td>
            </tr>

        </table>
        <hr/>

        <?= $content ?>

    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
