<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class ImportController
 * @package app\commands
 */
class ImportController extends Controller
{
    public $oldCountryTable     = 'trip_country';
    public $oldRegionTable      = 'trip_region';
    public $oldTownTable        = '100_town';
    public $oldTownPhotoTable   = 'trip_townphoto';
    public $countryTable        = '{{%geo_country}}';
    public $regionTable         = '{{%geo_region}}';
    public $townTable           = '{{%geo_town}}';
    public $townPhotoTable      = '{{%town_photo}}';

    public $oldUserTable        = 'trip_user';
    public $userTable           = '{{%user}}';
    public $oldUserPhotoTable   = 'trip_userphoto';
    public $userPhotoTable      = '{{%user_photo}}';

    public $oldCarTable         = 'trip_car';
    public $carTable            = '{{%car}}';
    public $carMarkTable        = '{{%car_mark}}';
    public $carModelTable       = '{{%car_model}}';
    public $oldCarPhotoTable    = 'trip_carphoto';
    public $carPhotoTable       = '{{%car_photo}}';

    public $oldTripTable        = 'trip_trip';
    public $tripTable           = '{{%trip}}';

    /** @var int лимит в запросах к большим таблицам */
    public $limit = 1000;

    /**
     * Импорт стран, регионов, городов, фоток городов
     *
     * @throws \yii\db\Exception
     */
    public function actionGeo()
    {
        // Страны
        $this->stdout("Truncate country table\n");
        Yii::$app->db->createCommand()->truncateTable($this->countryTable)->execute();

        $this->stdout("Importing countries\n");
        $list = (new Query())->from($this->oldCountryTable)->all(Yii::$app->db_old);
        $result = Yii::$app->db->createCommand()->batchInsert($this->countryTable, array_keys($list[0]), $list)->execute();
        $this->stdout("Done: $result\n\n");

        // Регионы
        $this->stdout("Truncate region table\n");
        Yii::$app->db->createCommand()->truncateTable($this->regionTable)->execute();

        $this->stdout("Importing regions\n");
        $list = (new Query())->from($this->oldRegionTable)->all(Yii::$app->db_old);

        $arr = [];
        foreach ($list as $item) {
            $arr[] = [
                'id'         => $item['id'],
                'name'       => $item['name'],
                'alias'      => $item['alias'],
                'country_id' => $item['countryId'],
                'buy_price'  => $item['buyPrice'],
                'sell_price' => $item['sellPrice'],
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert($this->regionTable, array_keys($arr[0]), $arr)->execute();
        $this->stdout("Done: $result\n\n");

        // Города
        $this->stdout("Truncate town table\n");
        Yii::$app->db->createCommand()->truncateTable($this->townTable)->execute();

        $this->stdout("Importing towns\n");
        $list = (new Query())->from($this->oldTownTable)->all(Yii::$app->db_old);

        $arr = [];
        foreach ($list as $item) {
            $arr[] = [
                'id'              => $item['id'],
                'name'            => $item['name'],
                'alias'           => $item['alias'],
                'country_id'      => $item['countryId'],
                'region_id'       => $item['regionId'],
                'description'     => $item['description'],
                'size'            => $item['size'],
                'seo_title'       => $item['seoTitle'],
                'seo_description' => $item['seoDescription'],
                'seo_keywords'    => $item['seoKeywords'],
                'photo'           => $item['photo'],
                'lat'             => $item['lat'],
                'lng'             => $item['lng'],
                'is_capital'      => $item['isCapital'],
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert($this->townTable, array_keys($arr[0]), $arr)->execute();
        $this->stdout("Done: $result\n\n");

        // Фото городов
        $this->stdout("Truncate town_photo table\n");
        Yii::$app->db->createCommand()->truncateTable($this->townPhotoTable)->execute();

        $this->stdout("Importing town photos\n");
        $list = (new Query())->from($this->oldTownPhotoTable)->all(Yii::$app->db_old);

        $arr = [];
        foreach ($list as $item) {
            $arr[] = [
                'id'         => $item['id'],
                'name'       => $item['name'],
                'town_id'    => $item['townId'],
                'main'       => $item['avatar'],
            ];
        }

        $result = Yii::$app->db->createCommand()->batchInsert($this->townPhotoTable, array_keys($arr[0]), $arr)->execute();
        $this->stdout("Done: $result\n\n");
    }

    /**
     * Импорт юзеров, фоток юзеров
     *
     * @throws \yii\db\Exception
     */
    public function actionUsers()
    {
        // Юзеры
        $this->stdout("Truncate user table\n");
        Yii::$app->db->createCommand()->truncateTable($this->userTable)->execute();

        $this->stdout("Importing users\n");
        $towns = $this->getTowns();
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldUserTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldUserTable)->distinct('email')->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $key = array_search(mb_strtolower($item['town']), $towns);
                $townId = $key > 0 ? $key : null;
                $arr[] = [
                    'id'            => $item['id'],
                    'name'          => $item['name'],
                    'password_hash' => $item['password'],
                    'email'         => $item['email'],
                    'confirm_code'  => $item['confirm_code'],
                    'hash'          => $item['hash'],
                    'login'         => $item['login'],
                    'description'   => $item['about'],
                    'gender'        => $item['gender'],
                    'role'          => $item['role'],
                    'rating'        => $item['rating'],
                    'vk_id'         => $item['vkId'],
                    'fb_id'         => $item['fbId'],
                    'car'           => $item['car'],
                    'phone'         => $item['phone'],
                    'town_id'       => $townId,
                    'email_access'  => $item['emailAccess'],
                    'phone_access'  => $item['phoneAccess'],
                    'created_at'    => $item['regdate'],
                    'status'        => $item['active'] == 1 ? 10 : 0,
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->userTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();

        // Фото юзеров
        $this->stdout("Truncate user_photo table\n");
        Yii::$app->db->createCommand()->truncateTable($this->userPhotoTable)->execute();

        $this->stdout("Importing user photos\n");
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldUserPhotoTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldUserPhotoTable)->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $arr[] = [
                    'id'            => $item['id'],
                    'name'          => $item['name'],
                    'user_id'       => $item['userId'],
                    'main'          => $item['avatar'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->userPhotoTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
    }

    /**
     * Импорт машин, фоток машин
     *
     * @throws \yii\db\Exception
     */
    public function actionCars()
    {
        // Машины
        $this->stdout("Truncate car table\n");
        Yii::$app->db->createCommand()->truncateTable($this->carTable)->execute();

        $this->stdout("Importing cars\n");
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldCarTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldCarTable)->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $arr[] = [
                    'id'            => $item['id'],
                    'manufacturer'  => $item['manufacturer'],
                    'model'         => $item['model'],
                    'year'          => $item['year'],
                    'user_id'       => $item['ownerId'],
                    'pollution'     => $item['pollution'],
                    'consumption'   => $item['consumption'],
                    'name'          => $item['nickname'],
                    'capacity'      => $item['capacity'],
                    'description'   => $item['description'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->carTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();

        // Фото машин
        $this->stdout("Truncate car_photo table\n");
        Yii::$app->db->createCommand()->truncateTable($this->carPhotoTable)->execute();

        $this->stdout("Importing car photos\n");
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldCarPhotoTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldCarPhotoTable)->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $arr[] = [
                    'id'            => $item['id'],
                    'name'          => $item['name'],
                    'car_id'        => $item['carId'],
                    'main'          => $item['avatar'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->carPhotoTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
    }

    /**
     * Привязка марок и моделей к машинам
     *
     * @throws \yii\db\Exception
     */
    public function actionMapCars()
    {
        $this->stdout("Map cars\n");

        $done = 0;
        $cars = (new Query())->select(['id', 'manufacturer', 'model'])->from($this->carTable)->all();
        $count = count($cars);
        $total = $count;
        $updated = 0;

        $list = (new Query())->select(['id', 'name'])->from($this->carMarkTable)->all();
        $marks = ArrayHelper::map($list, 'id', 'name');

        $list = (new Query())->select(['id', 'mark_id', 'name'])->from($this->carModelTable)->all();
        $models = ArrayHelper::map($list, 'id', 'name', 'mark_id');

        unset($list);

        Console::startProgress($done, $count);

        foreach ($cars as $car) {
            $markId = array_search($car['manufacturer'], $marks);
            if ($markId !== false) {
                $modelId = array_search($car['model'], $models[$markId]);
                if ($modelId !== false) {
                    $updated += Yii::$app->db->createCommand()->update($this->carTable, [
                        'mark_id' => $markId,
                        'model_id' => $modelId,
                        'manufacturer' => $marks[$markId],
                        'model' => $models[$markId][$modelId],
                    ], ['id' => $car['id']])->execute();
                }
            }

            $done++;
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
        $this->stdout("Машин всего/обновлено: $total/$updated\n");
    }

    /**
     * Импорт поездок
     *
     * @throws \yii\db\Exception
     */
    public function actionTrips()
    {
        // Поездки
        $this->stdout("Truncate trip table\n");
        Yii::$app->db->createCommand()->truncateTable($this->tripTable)->execute();

        $this->stdout("Importing trips\n");
        $towns = $this->getTowns();
        $done = 0;
        $offset = 0;
        $finish = false;
        $count = (new Query())->from($this->oldTripTable)->count('*', Yii::$app->db_old);

        Console::startProgress($done, $count);
        while (!$finish) {
            $list = (new Query())->from($this->oldTripTable)->leftJoin('trip_tripviews', 'id=tripId')->offset($offset)->limit($this->limit)->all(Yii::$app->db_old);

            $arr = [];
            foreach ($list as $item) {
                $from = array_search(mb_strtolower($item['townFrom']), $towns);
                $to = array_search(mb_strtolower($item['townTo']), $towns);
                $fromId = $from > 0 ? $from : 0;
                $toId = $to > 0 ? $to : 0;

                $status = 0;
                if ($item['active'] == 1) {
                    $status = 10;
                } else if ($item['complete'] == 1) {
                    $status = 20;
                }

                if ($fromId == 0 || $toId == 0) {
                    $status = 0;
                }

                $arr[] = [
                    'id'            => $item['id'],
                    'type'          => $item['driver'] > 0 ? 2 : 1,
                    'user_id'       => $item['autorId'],
                    'trip_time'     => $item['date'],
                    'town_from'     => $item['townFrom'],
                    'town_to'       => $item['townTo'],
                    'from_id'       => $fromId,
                    'to_id'         => $toId,
                    'driver_id'     => $item['driver'] > 0 ? $item['driver'] : null,
                    'passengers'    => $item['passengers'],
                    'description'   => $item['comment'],
                    'price'         => $item['price'] < 1000000 ? $item['price'] : 0,
                    'periodic'      => $item['periodic'],
                    'car_id'        => $item['carId'],
                    'views'         => $item['views'] ? $item['views'] : 0,
                    'status'        => $status,
                    'created_at'    => $item['created'],
                ];
            }

            $result = Yii::$app->db->createCommand()->batchInsert($this->tripTable, array_keys($arr[0]), $arr)->execute();
            $done += $result;
            $offset += $result;
            if ($result < $this->limit) {
                $finish = true;
            }
            Console::updateProgress($done, $count);
        }

        Console::endProgress();
    }

    /**
     * Массив городов id => name
     *
     * @return array
     */
    protected function getTowns()
    {
        $townsArr = (new Query())->select('id, TRIM(LOWER(name)) AS name')->from($this->townTable)->all();
        return ArrayHelper::map($townsArr, 'id', 'name');
    }
}