<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;

$menuItems = [];
if (Yii::$app->user->isGuest) {
    $menuItems = [
        ['label' => 'Вход', 'url' => ['/site/login']],
        ['label' => 'Регистрация', 'url' => ['/site/register']],
    ];
} else {
    $menuItems = [
        [
            'label' => 'Профиль',
            'url' => ['/profile/view'],
        ],
        [
            'label' => 'Выход (' . $user->name . ')',
            'url' => '/site/logout/',
            'linkOptions' => ['data-method' => 'post']
        ],
    ];
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= $this->title ?></title>
    <?php $this->head() ?>
    <script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap container">
        <div class="container">
            <div class="jumbotron big-banner">
                <img src="/img/logo_bar.png" class="vert-margin40">
                <p class="lead home-title">Уже 58495 человек из России, Украины и Беларуси используют наш сайт чтобы экономить на
                    поездках на работу, учебу или путешествиях между городами </p>

            </div>
        </div>

    <div class="">
        <?php NavBar::begin([
            'brandLabel' => 'Попутчики.ру',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-default',
            ],
        ]); ?>
        <?php echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => \yii\helpers\ArrayHelper::merge([
                ['label' => 'Попутчики на автомобиле из города в город', 'url' => ['#']],
                ['label' => 'Попутчики отпуск заграницу', 'url' => ['#']],
                ['label' => 'Калькулятор расхода топлива', 'url' => ['/site/calculator']],
            ], $menuItems)
        ]); ?>
        <?php NavBar::end(); ?>
    </div>

    <div class="container">
        <!--<div class="header">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <?= Html::a(Html::img('/img/logo_bar.png', ['alt' => 'Поехали вместе!']), '/'); ?>
                </div>
                <div class="col-md-6 col-sm-6 hidden-xs">
                    <div class="text" style="text-align: center;">Сервис поиска попутчиков</div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="text pull-right">
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <?= Html::a('Вход', ['/site/login']); ?>
                            |
                            <?= Html::a('Регистрация', ['/site/register']); ?>
                        <?php } else { ?>
                            <?= Html::a(Html::encode($user->email), ['/profile/view']); ?>
                            |
                            <?= Html::a('Выход', ['/site/logout'], ['data-method' => 'post']); ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>-->

        <?= Breadcrumbs::widget([
            'encodeLabels' => false,
            'homeLink' => [
                'label' => 'Попутчики',
                'url' => '/',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <h5>Администрация проекта</h5>
                info@povm.ru<br/>
                2012 ©
            </div>
            <div class="col-md-2 col-sm-2">
                <h5>Мы в соцсетях</h5>
            </div>
            <div class="col-md-2 col-sm-2">
                <h5>О сайте</h5>
                <ul>
                    <li><?= Html::a('Автопарк проекта', ['/auto/index']); ?></li>
                    <li><?= Html::a('Техника безопасности', ['/site/safety']); ?></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <h5>Информация</h5>
                <ul>
                    <li><?= Html::a('Как это работает', ['/site/about']); ?></li>
                    <li><?= Html::a('Правила сервиса', ['/site/rules']); ?></li>
                    <li><?= Html::a('Обратная связь', ['/site/feedback']); ?></li>
                    <li><?= Html::a('Наши партнеры', ['/site/partners']); ?></li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-2">
                <h5>География</h5>
                <?= \app\widgets\CountryWidget::widget(); ?>
            </div>
            <div class="col-md-2 col-sm-2">
                Счетчик
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
