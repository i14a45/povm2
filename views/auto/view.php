<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\models\Car */

$this->title = 'Автомобиль ' . $model->getTitle();

$this->params['breadcrumbs'][] = [
    'label' => 'Автомобили',
    'url' => ['/auto/index'],
];

$this->params['breadcrumbs'][] = $this->title;

$town = $model->user->town;
?>

<div class="auto-view">
    <div class="row">
        <div class="col-md-12 col-sm-12">
        <h1><?= Html::encode($this->title); ?></h1>
        </div>
        <div class="col-md-6 cols-sm-6">

            <h2>Владелец</h2>
            <?= Html::a(
                Html::img('/img/thumbs/user.jpg', [
                    'class' => 'avatar',
                    'alt' => Html::encode($model->user ? $model->user->name : 'unknown'),
                ]), ['/profile/view', 'id' => $model->user->id]
            ); ?>
            <?= Html::a(Html::encode($model->user->name), ['/profile/view', 'id' => $model->user->id]); ?>
            <?php if ($town) { ?>
                <?= Html::a(Html::encode($town->name), $town->getUrl()); ?>
            <?php } ?>

            <h2>Описание машины</h2>
            <?= 'Год выпуска: ' . $model->year . '<br/>'; ?>
            <?php if ($model->description) { ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $model->description; ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-6 cols-sm-6">
            <h2>Фотографии машины</h2>
        </div>
    </div>
</div>
