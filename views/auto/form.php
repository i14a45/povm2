<?php

use app\models\Car;
use app\models\CarMark;
use app\models\CarModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model Car */

$this->title = $model->isNewRecord ? 'Добавление автомобиля' : 'Редактирование автомобиля';

$this->params['breadcrumbs'][] = [
    'label' => 'Автомобили',
    'url' => ['/auto/index'],
];

$this->params['breadcrumbs'][] = [
    'label' => $model->getTitle(),
    'url' => ['/auto/view', 'id' => $model->id],
];

$this->params['breadcrumbs'][] = 'Редактирование';


?>

<div class="auto-form">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h1><?= Html::encode($this->title); ?></h1>
        </div>

        <?php $form = ActiveForm::begin([]); ?>

        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'mark_id')->dropDownList(CarMark::getDropdownList(), [
                'class' => 'form-control select2',
                'id' => 'car-mark',
                'prompt' => '- Выбрать -',
            ]); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'model_id')->dropDownList(CarModel::getDropdownList($model->mark_id), [
                'id' => 'car-model',
                'prompt' => '- Выбрать -',
            ]); ?>
        </div>

        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'year')->dropDownList(Car::getYears(), [
                'class' => 'form-control select2',
                'prompt' => '- Выбрать -',
            ]); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'capacity')->textInput(); ?>
        </div>
        <div class="col-md-12 col-sm-12">
            <?= $form->field($model, 'description')->textarea(); ?>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
