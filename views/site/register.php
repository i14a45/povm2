<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\RegistrationForm */

$title = 'Регистрация';

$this->title = Yii::$app->name . ' - ' . $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="site-register">
    <h1><?= Html::encode($title) ?></h1>

    <div class="row">
        <div class="col-md-6 col-sm-6">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
            ]); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?= Html::a('Войти', ['/site/login']); ?>
            |
            <?= Html::a('Забыли пароль?', ['/site/restore-password']); ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
