<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$title = 'Вход';

$this->title = Yii::$app->name . ' - ' . $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="site-login">
    <h1><?= Html::encode($title) ?></h1>

    <div class="row">
        <div class="col-md-6 col-sm-6">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
            ]); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?= Html::a('Регистрация', ['/site/register']); ?>
                |
                <?= Html::a('Забыли пароль?', ['/site/restore-password']); ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
