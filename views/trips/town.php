<?php

use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \app\models\GeoCountry|null $country */
/* @var \app\models\GeoRegion|null $region */
/* @var \app\models\GeoTown|null $town */

$title = Html::encode($town->name);
$this->title = 'Попутчики из/в ' . $title . ' на машине';

$this->params['breadcrumbs'][] = [
    'label' => Html::encode($country->name),
    'url'   => $country->getUrl(),
];

$this->params['breadcrumbs'][] = [
    'label' => Html::encode($region->name),
    'url'   => $region->getUrl(),
];

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="trips-town">
    <div class="town-photo">
        photo
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <h1>Объявления о поиске попутчиков из/в <?= $title; ?></h1>
            <?= $this->render('_trips'); ?>
        </div>
        <div class="col-md-4 col-sm-4">
            Попутчики из этого города
        </div>
    </div>
</div>
