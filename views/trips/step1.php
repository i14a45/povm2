<?php

use app\models\Trip;
use yii\helpers\Html;

/* @var $this View */
/* @var $model \app\models\Step1Form */

$this->title = 'Новая поездка';

use yii\web\View;
use yii\widgets\ActiveForm; ?>

<div class="row">
    <div class="col-md-8 col-sm-8 vert-margin40">
        <h1><?= $this->title; ?></h1>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group">
            <?= Html::submitButton('Я пассажир', [
                'class' => 'btn btn-success select-role-btn',
                'name' => 'Step1Form[type]',
                'value' => Trip::TYPE_FIND_DRIVER,
            ]); ?>
            <?= Html::submitButton('Я водитель', [
                'class' => 'btn btn-success select-role-btn',
                'name' => 'Step1Form[type]',
                'value' => Trip::TYPE_FIND_PASSENGERS
            ]); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
