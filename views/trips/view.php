<?php

use yii\helpers\Html;
use app\components\StringHelper;
use app\models\Trip;

/* @var $this yii\web\View */
/* @var $model \app\models\Trip */

$passengerIcon = '<div class="trip-icon" title="Кол-во пассажиров"><i class="fas fa-user"></i></div>';
$carIcon = '<div  class="trip-icon" title="Автомобиль"><i class="fas fa-car"></i></div>';
$clockIcon = '<div class="trip-icon" title="Время выезда"><i class="fas fa-clock"></i></div>';
$rubIcon = '<div class="trip-icon" title="Стоимость поездки"><i class="fas fa-ruble-sign "></i></div>';
$viewsIcon = '<div class="trip-icon" title="Просмотров"><i class="fas fa-eye "></i></div>';

$townFrom = Html::encode($model->town_from);
$townTo = Html::encode($model->town_to);

$this->title = 'Попутчики ' . $townFrom . ' - ' . $townTo . '. На машине ' .
    StringHelper::mb_ucfirst(Yii::$app->formatter->asDate($model->trip_time, 'php:D d M Y H:i')) . '. ' .
    Yii::$app->name;

$this->params['breadcrumbs'][] = [
    'label' => 'Объявления',
    'url' => ['/trips/index']
];
$this->params['breadcrumbs'][] = $townFrom . ' - ' . $townTo;

$car = $model->car ? Html::encode($model->car->manufacturer) . '&nbsp;' . Html::encode($model->car->model) : '';
$userName = Html::encode($model->user->name);

\app\assets\YMapsAsset::register($this);

?>

    <div class="row">
        <div class="col-md-8 col-sm-8 vert-margin40">
            <h1><?= 'Попутчики' . ' ' . $townFrom . '&nbsp;&rarr;&nbsp;' . $townTo; ?></h1>
            <div class="row vert-margin40">
                <div class="col-md-4 col-sm-1 center-align">
                    <?= Html::a(
                        Html::img('/img/thumbs/user150.jpg', [
                            'alt' => Html::encode($model->user->name),
                        ]), ['/profile/view', 'id' => $model->user->id]
                    ); ?>
                </div>
                <div class="col-md-8 col-sm-11">
                    <h2>Информация о поездке:</h2>

                    <div class="lead">
                        <?= Html::a($userName, ['/profile/view', 'id' => $model->user->id]); ?> ищет
                        <?= $model->type === Trip::TYPE_FIND_PASSENGERS ? 'пассажиров' : 'водителя'; ?> на
                        <u><?= StringHelper::mb_ucfirst(Yii::$app->formatter->asDate($model->trip_time, 'php:D d M Y')); ?></u>
                    </div>
                    <?= \app\widgets\TripIconsWidget::widget([
                        'model' => $model,
                    ]); ?>
                </div>
            </div>

            <?php if (!empty($model->description)) { ?>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h2>Комментарий автора к поездке
                        </h2>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?= $model->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h2>Маршрут на карте (ориентировочный)</h2>
                    <p><strong>*Маршрут строится автоматически и может отличаться от реального!</strong></p>
                    <div id="ymap" style="width:100%;height:400px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 hidden-xs">
            <button type="button" class="btn btn-lg btn-block btn-default">Откликнуться</button>
            <button type="button" class="btn btn-lg btn-block btn-default">Создать свою поездку</button>
        </div>
    </div>


<?php
$townFrom = $model->town_from;
$townTo = $model->town_to;
$this->registerJs(
    <<<JS

var map;
var townFrom = "$townFrom";
var townTo = "$townTo";

ymaps.ready(initMap);

function initMap()
{
    map = new ymaps.Map('ymap', {
        center: [55.76, 37.64],
        zoom: 10
    }, {
        searchControlProvider: 'yandex#search'
    });
    
    var route = new ymaps.multiRouter.MultiRoute({   
        referencePoints: [
            townFrom,
            townTo
        ]
    }, {
        boundsAutoApply: true
    });
    
    map.geoObjects.add(route);
    
    route.events.once('update', function() {
        route.getActiveRoute().balloon.open();
    });
    
}

JS
);