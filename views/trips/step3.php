<?php

use app\models\Step3Form;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Step3Form */

$this->title = 'Новая поездка';
?>

<div class="row">
    <div class="col-md-8 col-sm-8 vert-margin40">
        <h1><?= $this->title; ?></h1>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn btn-primary',
            ]) ?>
        </div>

        <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'fromId')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'toId')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'date')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'carMarkId')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'carModelId')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'carYear')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'wayBack')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'dateBack')->hiddenInput()->label(false) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
