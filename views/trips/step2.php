<?php

use app\models\Car;
use app\models\CarMark;
use app\models\CarModel;
use app\models\Step2Form;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Step2Form */

$this->title = 'Новая поездка';

?>

<div class="row">
    <div class="col-md-8 col-sm-8 vert-margin40">
        <h1><?= $this->title; ?></h1>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model) ?>

        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'fromId')->dropDownList([], [
                    'prompt' => 'Откуда едем',
                    'class' => 'form-control select-town',
                    'data' => [
                        'ajax--url' => Url::to(['/site/town'], true),
                    ],
                ])->label(false) ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'toId')->dropDownList([], [
                    'prompt' => 'Куда едем',
                    'class' => 'form-control select-town',
                    'data' => [
                        'ajax--url' => Url::to(['/site/town'], true),
                    ],
                ])->label(false) ?>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <?= $form->field($model, 'date')->widget(DateTimePicker::class, [
                        'type' => DateTimePicker::TYPE_INPUT,
                        'options' => [
                            'placeholder' => 'Когда',
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy hh:ii',
                            'language' => 'ru',
                        ]
                    ])->label(false); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'wayBack')->checkbox(); ?>
            </div>
            <div class="col-lg-4 date-back" style="display:none;">
                <div class="form-group">
                    <?= $form->field($model, 'dateBack')->widget(DateTimePicker::class, [
                        'type' => DateTimePicker::TYPE_INPUT,
                        'options' => [
                            'placeholder' => 'Когда',
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy hh:ii',
                            'language' => 'ru',
                        ]
                    ])->label(false); ?>
                </div>
            </div>
        </div>

        <?php if ($model->type == \app\models\Trip::TYPE_FIND_PASSENGERS) { ?>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <?= $form->field($model, 'carMarkId')->dropDownList(CarMark::getDropdownList(), [
                        'class' => 'form-control select2',
                        'id' => 'car-mark',
                        'prompt' => '- Выбрать -',
                    ]); ?>
                </div>
                <div class="col-md-4 col-sm-4">
                    <?= $form->field($model, 'carModelId')->dropDownList(CarModel::getDropdownList($model->carMarkId), [
                        'class' => 'form-control select2',
                        'id' => 'car-model',
                        'prompt' => '- Выбрать -',
                    ]); ?>
                </div>

                <div class="col-md-4 col-sm-4">
                    <?= $form->field($model, 'carYear')->dropDownList(Car::getYears(), [
                        'class' => 'form-control select2',
                        'prompt' => '- Выбрать -',
                    ]); ?>
                </div>
            </div>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton('Далее', [
                'class' => 'btn btn-primary',
            ]) ?>
        </div>

        <?= $form->field($model, 'type')->hiddenInput()->label(false); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
