<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \app\models\User */

$userName = Html::encode($model->name);
$town = $model->town;
$this->title = 'Профиль пользователя ' . $userName;

$userMenu = [
    Html::a('<i class="fas fa-camera"></i> Загрузить фото',' #'),
    '',
    Html::a('<i class="fas fa-pencil-alt"></i> Редактировать', ['/profile/update']),
    Html::a('<i class="fas fa-lock"></i> Сменить пароль', ['/profile/change-password']),
    Html::a('<i class="fas fa-exclamation-circle "></i> Заблокировать', ['/profile/block'], ['class' => 'text-danger']),
];

?>

<div class="profile-view">
    <div class="row">
        <div class="col-md-2 col-sm-2">
            <?= Html::img('/img/thumbs/user150.jpg', [
                'class' => 'img-responsive',
                'alt' => $userName . ($town ? ' ' . Html::encode($town->name) : ''),
            ]); ?>
            <h2>Профиль</h2>
            <?= Html::ul($userMenu, [
                'encode' => false,
                'style' => 'list-style-position:inside;',
            ]); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <h1><?= $userName; ?> <i class="fas fa-mars"></i></h1>

            <?php if ($town) { ?>
                <?= '<i class="fas fa-map-marker-alt"></i> ' . Html::a(Html::encode($town->name), $town->getUrl()); ?>
            <?php } ?>

            <?php if ($model->description) { ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $model->description; ?>
                    </div>
                </div>
            <?php } ?>

            <?= \app\widgets\GarageWidget::widget([
                'user' => $model,
            ]); ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= \app\widgets\TripHistory::widget([
                'userId' => $model->id,
            ]); ?>
        </div>
    </div>
</div>